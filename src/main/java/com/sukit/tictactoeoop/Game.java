/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukit.tictactoeoop;

import java.util.Scanner;

/**
 *
 * @author Xenon
 */
public class Game {

    Player playerX;
    Player playerO;
    Player turn;
    Table table;
    boolean isRunning;
    int row, col;
    Scanner scan = new Scanner(System.in);

    public Game() {
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX, playerO);
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showTable() {
        table.showTable();
    }

    public void input() {
        while (true) {
            System.out.println("Please input Row Col:");
            row = scan.nextInt() - 1;
            col = scan.nextInt() - 1;
            if (table.setRowCol(row, col)) {
                break;
            }
            System.out.println("");
        }
    }

    public void showTurn() {
        System.out.println(table.getCurrentPlayer().getName() + " turn");
    }

    public void newGame() {
        table = new Table(playerX, playerO);
    }

    public void showBye() {
        System.out.println("Bye bye...");
    }

    public void askRestart() {
        boolean isRestart = false;
        do {
            System.out.println("Do you want to play again?(Y/N): ");
            String restart = scan.next();
            if (restart.equalsIgnoreCase("Y")) {
                newGame();
            } else if (restart.equalsIgnoreCase("N")) {
                isRunning = false;
                showBye();
            } else {
                isRestart = true;
            }
        } while (isRestart);
    }
    
    public void run() {
        isRunning = true;
        this.showWelcome();
        do {
            this.showTable();
            this.showTurn();
            this.input();
            table.increaseCounter();
            table.checkWin();
            checkFinish();
            table.turnCycle();
        }while (isRunning);
    }

    private void checkFinish() {
        if (table.checkFinish()) {
            this.showTable();
            if (table.getWinner() == null) {
                System.out.println("Draw!!");
            } else {
                System.out.println(table.getWinner().getName() + " Win!!");
            }
            askRestart();
        }
    }
}
